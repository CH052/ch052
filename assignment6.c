#include <stdio.h>
int main()
{
  int n, r = 0, t;
  printf("Enter a number to check if its palindrome or not");
  scanf("%d",&n);
  t = n;
  while (t != 0)
{
    r = r * 10;
    r = r + t%10;
    t = t/10;
}

  if (n == r)
  printf("%d is a palindrome", n);
  else
  printf("%d is not a palindrome", n);
  return 0;
}