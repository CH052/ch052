#include <stdio.h>
int main()
{
   int arr[10], pos, i, n;
   printf("\n Enter number of elements in array:");
   scanf("%d", &n);
   printf("\n Enter elements of array:");
   
   for (i = 0; i < n; i++)
   scanf("%d", &arr[i]);
   printf("\n Enter the position from which the number has to be deleted:");
   scanf("%d", &pos);
   
   for(i= pos; i<n-1; i++)
   arr[i] = arr[i+1];
   n--;
   printf("\n The array after deletion is:");
   for(i=0; i<n; i++)
   printf("\n Arr[%d]= %d", i, arr[i]);
   return 0;
}