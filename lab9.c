#include <stdio.h>
int main()
{
  int r, c, a[100][100], b[100][100], sum[100][100], i, j;
  printf("\n Enter the number of rows: ");
  scanf("%d", &r);
  printf("\n Enter the number of columns: ");
  scanf("%d", &c);

  printf("\n Enter elements of 1st matrix:\n");
  for (i = 0; i < r; ++i)
  for (j = 0; j < c; ++j)
  {
   printf("\n Enter element a%d%d: ", i + 1, j + 1);
   scanf("%d", &a[i][j]);
  }
    printf("\n Enter elements of 2nd matrix:\n");
    for (i = 0; i < r; ++i)
    for (j = 0; j < c; ++j)
    {
     printf("\n Enter element a %d%d: ", i + 1, j + 1);
     scanf("%d", &b[i][j]);
    }
     for (i = 0; i < r; ++i)
     for (j = 0; j < c; ++j)
     {
      sum[i][j] = a[i][j] + b[i][j];
     }
     printf("\n Sum of two matrices:");
     for (i = 0; i < r; ++i)
     for (j = 0; j < c; ++j)
     {
      printf("\n %d   ", sum[i][j]);
      if (j == c - 1)
      {
        printf("\n\n");
      }
     }
    return 0;
}