# include <stdio.h>
int main()
{
  int a, b, c, biggest_no; 
  printf("\n Enter three numbers: ");
  scanf("%d %d %d", &a, &b, &c);
  biggest_no= a > b ? (a > c ? a : c) : (b > c ? b : c);
  printf("\n The biggest number is : %d", biggest_no);
  return 0;
}