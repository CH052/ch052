#include <stdio.h>
int main()
{
    float num1, num2;   
    float *ptr1, *ptr2;  
    float sum, diff, mult, div;
    ptr1 = &num1; 
    ptr2 = &num2; 
    printf("\n Enter any two real numbers: ");
    scanf("%f%f", ptr1, ptr2);
    sum  = (*ptr1) + (*ptr2);
    diff = (*ptr1) - (*ptr2);
    mult = (*ptr1) * (*ptr2);
    div  = (*ptr1) / (*ptr2);
    printf("Sum = %f \n", sum);
    printf("Difference = %f \n", diff);
    printf("Product = %f \n", mult);
    printf("Quotient = %f \n", div);
    return 0;
}