#include <stdio.h>
void swap(int*, int*);
int main()
{
   int x, y;
   printf("\n Enter the value of x and y:");
   scanf("%d%d",&x,&y);
   printf("\n Before Swapping\nx = %d\ny = %d", x, y);
   swap(&x, &y); 
   printf("\n After Swapping\nx = %d\ny = %d", x, y);
   return 0;
}
  void swap(int *a, int *b)
  {
    int temp;
    temp = *b;
    *b = *a;
    *a = temp;   
}
 
